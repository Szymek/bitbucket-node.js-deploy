﻿var http = require('http');
var https = require("https");
var fs = require("fs");
var urlLib = require("url");
var AdmZip = require("adm-zip");

/////
var port = process.env.port || 7249;
var branch = "master";
var path = "/var/www/node";
/////

function download(url, cb) {
	var file;
	var options = urlLib.parse(url);
	options.rejectUnauthorized = false;
	options.agent = new https.Agent(options);

	var request = https.get(options, cb);
	
	request.on('error', function (e) {
		console.log("Error: " + e.message);
	});
}
function sendResponse(res, httpCode, cType, output) {
	res.writeHead(httpCode, { "Content-Type": cType ? cType : "text/plain" });
	res.end(output);	
}
http.createServer(function (req, res) {
	if (req.method == "POST") {
		var post = "";
		req.on("data", function (d) {
			post += d;
		});
		req.on("end", function () {						
			var body;
			try {
				body = JSON.parse(post);
			}
			catch (exception) {
				sendResponse(res, 403, false, "Malformed data");
				console.log("Malformed POST");
				return;
			}
			
			sendResponse(res, 200, false, "OK");
			
			var changes = body.push.changes[0].new;
			if (changes.name == branch) {
				var zipUrl = changes.repository.links.html.href + "/get/HEAD.zip";

				//dl
				download(zipUrl, function (resp) {
					var tmpZipName = "./" + Math.random() + ".zip";
					var file = fs.createWriteStream(tmpZipName);
					file.on("error", function() {
						console.log("error");
					});
					file.on("close", function() {
						console.log("Extracting...");
						var zip = new AdmZip(tmpZipName);
						var entries = zip.getEntries();
						zip.extractEntryTo(entries[0].entryName, path, false, true);
						console.log("Updated!");
						fs.unlink(tmpZipName);
					});
					return resp.pipe(file);
				});
			}
			else
				console.log(changes.name + " ignored");
		});
	}
	else {
		sendResponse(res, 403, false, "Forbidden");
	}
}).listen(port);